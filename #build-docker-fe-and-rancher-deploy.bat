@ECHO off

set servicename=sdtooling-wizard-fe

ECHO ============================
ECHO SERVICE NAME : %servicename%
ECHO ============================

ECHO ============================
ECHO GIT PULL
ECHO ============================

call git pull

ECHO ============================
ECHO BUILD FE
ECHO ============================

call cd implementation\
 
call npm install
 
call ng build --configuration=production

call cd ..\

ECHO ============================
ECHO DOCKER LOGIN
ECHO ============================

call docker login code.europa.eu:4567

ECHO ============================
ECHO DOCKER IMAGE PRUNE
ECHO ============================

call docker image prune

ECHO ============================
ECHO DOCKER BUILD
ECHO ============================

call docker build -t code.europa.eu:4567/simpl/simpl-open/development/data1/%servicename% .

ECHO ============================
ECHO DOCKER PUSH
ECHO ============================

call docker push code.europa.eu:4567/simpl/simpl-open/development/data1/%servicename%

ECHO ============================
ECHO GO TO HELM DIR
ECHO ============================

call cd deployment\sd\helm\sd-creation-wizard\

ECHO ============================
ECHO RANCHER UNINSTALL
ECHO ============================

call helm uninstall sd

ECHO ============================
ECHO RANCHER INSTALL
ECHO ============================

call helm install sd .

ECHO ============================
ECHO RETURN TO ROOT DIR
ECHO ============================

call cd ..\..\..\..\

ECHO ============================
ECHO ALL COMMAND COMPLETED
ECHO ============================

EXIT /B